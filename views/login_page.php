<?php
    include_once $_SERVER['DOCUMENT_ROOT'].'/reusables/Header.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/reusables/Footer.php';

    $header = new Header("Login");
    $header->createHeader();
    echo $header->getHeader();
?>
        <div class="section">
            <div class="row">
                <form action="../authorization/login.php" method="post" class="col s12">
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="text" name="login" id="login-field" class="validate" required>
                            <label for="login-field">Login</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input type="password" name="password" id="password-field" class="validate" required>
                            <label for="password-field">Password</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light" type="submit" name="login-btn">Proceed
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

<?php
    $footer = new Footer();
    $footer->createFooter();
    echo $footer->getFooter();
?>