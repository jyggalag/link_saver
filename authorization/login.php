<?php
session_start();

include_once $_SERVER['DOCUMENT_ROOT'].'/config/Database.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/config/functions.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/api/objects/User.php';

if (empty($_POST['login']) || empty($_POST['password'])) {
    session_destroy();
    badLoginRedirect("../views/login_page.php", "Missing credentials");
}

$database = new Database();
$db = $database->getConnection();

$user = new User($db);
$login = $_POST['login'];
$password = $_POST['password'];
$password = sha1(sha1($password));

$user->login = $login;
$user->password = $password;
$user->getUser();

if ($user->login == null) {
    session_destroy();
    badLoginRedirect("../views/login_page.php", "Wrong login or password");
} else {
    $_SESSION['user'] = $user->login;
    redirect("../");
}




