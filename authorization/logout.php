<?php 
session_start();

include_once $_SERVER['DOCUMENT_ROOT']."/config/functions.php";

unset($_SESSION['user']);
session_destroy();
redirect("../");