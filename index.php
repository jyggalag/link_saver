<?php
   
    session_start();
    
    if (empty($_SESSION['user'])) {
        header('Location: views/login_page.php');
    }

    include_once $_SERVER['DOCUMENT_ROOT'].'/reusables/Header.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/reusables/Footer.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/api/category/read.php';

    // echo $categories_arr;

    $header = new Header("Links adder");
    $header->createHeader($_SESSION['user']);
    echo $header->getHeader();
?>

    <br />
    <form class="col s12" action="" method="post" id="add-link-form">
        <div class="row">
            <div class="input-field col s3">
                <input id="title" name="title" type="text" class="validate" required>
                <label for="title">title</label>
            </div>
        

            <div class="input-field col s3" required>
                <select id="categories-selector" name="category_id">
                    <option value="" disabled selected>Choose category:</option>
                    <?php  
                        foreach($categories_arr as $key => $val) {
                            echo '<option value="'.$val['id'].'">'.$val['name'].'</option>';
                        }
                    ?>
                </select>
                <label>Category</label>
            </div>

            <div class="input-field col s6">
                <input id="link" name="url" type="text" class="validate" required>
                <label for="link">link</label>
            </div>
        </div>

        <p>
            <button class="waves-effect waves-light btn right" type="submit">Add link</button>
            <button class="waves-effect waves-light btn left modal-trigger" data-target="modal1">Add new category</button>
        </p>
    </form>
    <br /><br /><br />
    <hr />

    <div id="app">

    </div>



    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Adding category</h4>
            <form class="col s12" action="" method="post" id="add-category-form">
                <div class="row">
                    <div class="input-field col s3">
                        <input name="name" id="name" type="text" class="validate" required>
                        <label for="name">Category name</label>
                    </div>
                  </div>
                <p>
                    <button class="waves-effect waves-light btn left" type="submit">Add category </button>
                </p>
            </form>
        </div>
        <div class="modal-footer">
            <a href="#" id="btn-cancel" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
        </div>
    </div>


    <div id="modal2" class="modal">
        <div class="modal-content">
            <h4 class="center">Are you sure you want to delete link?</h4>
            <form id="delete-link-form" type="post">
                <p class="right">
                    <button type="submit" class="waves-effect waves-light btn modal-close" id="btn-yes">Yes <i class="material-icons right">check</i></button>
                    <button type="reset" class="waves-effect waves-light btn modal-close">No <i class="material-icons right">cancel</i></button>
                </p>
            </form>

        </div>
        <div class="modal-footer">

        </div>
    </div>


<?php
    $footer = new Footer();
    $footer->createFooter();
    echo $footer->getFooter();
?>