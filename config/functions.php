<?php

function redirect($url, $statusCode = 303)
{
   header('Location: ' . $url, true, $statusCode);
   die();
}

function badLoginRedirect($uri, $message) {
    echo "<script>alert('".$message."');</script>";
    redirectJs($uri);
}

function redirectJs($uri) {
    echo '
        <script>window.location.replace("'.$uri.'");</script>
    ';

    die();
}

