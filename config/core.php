<?php
// error reportings
ini_set('display_errors', 1);
error_reporting(E_ALL);


// homepage Url
$home_url = "http://localhost:8000/api";

// page given in URL paramater, default page is one
$page = isset($_GET['page']) ? $_GET['page'] : 1;

// set number of records per page
$records_per_page = 6;

// calculate fpr the query LIMIT clause
$from_record_num = ($records_per_page * $page) - $records_per_page;
