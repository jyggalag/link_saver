<?php

class Utilities {
    public static function getPaging(int $page, int $total_rows, int $records_per_page, string $page_url): array {
        $paging_arr = array();

        // first page button
        $paging_arr["first"] = $page > 1 ? "{$page_url}page=1" : "";

        $total_pages = ceil($total_rows / $records_per_page);

        // range of links
        $range = 2;

        // below and above current page links
        $initial_num = $page - $range;
        $condition_limit_num = ($page + $range) + 1;

        $paging_arr["pages"] = array();
        $page_count = 0;

        // middle pages buttons
        for ($i = $initial_num; $i < $condition_limit_num; $i++) {
            if (($i > 0) && ($i <= $total_pages)) {
                $paging_arr["pages"][$page_count]["page"] = $i;
                $paging_arr["pages"][$page_count]["url"] = "{$page_url}page={$i}";
                $paging_arr["pages"][$page_count]["current_page"] = $i == $page ? "yes" : "no";

                $page_count++;
            }
        }

        //last page button
        $paging_arr["last"] = $page < $total_pages ? "{$page_url}page={$total_pages}" : "";

        // return array
        return $paging_arr;
    }
}