<?php

class Database {

    private $host = "mysql";
    private $dbName = "link_saver";
    private $userName = "user123";
    private $password = "zaq1@WSX";

    public $conn;

    public function getConnection() {
        $this->conn = null;

        try {
            $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$this->dbName, $this->userName, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            echo "Connection error: ".$e->getMessage();
        }

        return $this->conn;
    }
}