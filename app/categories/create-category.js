$(document).on('submit', '#add-category-form', function() {
    var form_data = JSON.stringify($(this).serializeObject());

    $.ajax({
        type: "POST",
        url: "http://localhost:8000/api/category/create.php",
        data: form_data,
        contentType: "application/json; charset=ISO-8859-2",
        success: function (result) {
            $("#name").val('');
            location.reload();
            M.toast({html: 'Category created!'})
        },
        error: function(xhr, response, text) {
            console.log(xhr, response, text);
            console.warn(xhr.responseText);
        }
    });
    return false;
});
