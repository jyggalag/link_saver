$(document).ready(function() {
    showLinksFirstPage();
    $(document).on('click', '.pagination li', function() {
        var json_url = $(this).find('a').attr('data-page');
        showLinks(json_url);
    });
});

function showLinksFirstPage() {
    var json_url = "http://localhost:8000/api/link/read.php";
    showLinks(json_url);
}

function showLinks(json_url) {
    $.getJSON(json_url, function(data) {
        $("#links-count").text(data.records.length);
        readLinksTemplate(data);
    });
}