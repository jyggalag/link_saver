function createTableHead() {
    return `<div id="links">
    <table class="responsive-table">
        <thead>
            <tr>
                <th>starred</th>
                <th>Title</th>
                <th>Category</th>
                <th>Date added</th>
                <th>Refference</th>
                <th>Delete</th>
            </tr>
        </thead>

        <tbody>`;
}

function createTableClose() {
    return `</tbody></table></div>`;
}


function readLinksTemplate(data) {
    var read_links_html = createTableHead();
            $.each(data.records, function(key, val) {
                star_status = `star_border`;

                if (val.starred == '1') {
                    star_status = `star`;
                }

                if (val.starred == `0`) {
                    star_status = `star_border`
                }

                read_links_html +=`
                    <tr>
                        <td><i class="small material-icons" id="star" data-id="` + val.id + `">` + star_status + `</i></td>
                        <td>` + val.title + `</td>
                        <td>` + val.category_name + `</td>
                        <td>` + val.added_at + `</td>
                        <td><a href="` + val.url + `" class="waves-effect waves-light btn" target="_blank">GO <i class="material-icons right">send</i></a></td>
                        <td><a class="modal-trigger" data-target="modal2" id="delete" title="delete" data-id="` + val.id + `" href="#" ><i class="small material-icons">delete_forever</i></a></td>
                    </tr>`;
            });

         read_links_html += createTableClose();

    if (data.paging) {
        read_links_html += `<div id="paging"><ul class="pagination">`;

        // first page
        if (data.paging.first != "") {
            read_links_html += `<li><a href="#" data-page="` + data.paging.first + `"><i class="material-icons">first_page</i></a></li>`;
        }

        $.each(data.paging.pages, function(key, val) {
            var active_page = val.current_page == "yes" ? "active" : "";
            read_links_html += `<li class="waves-effect `+ active_page +`"><a href="#" data-page="` + val.url + `">` + val.page + `</a></li>`
        });

        if (data.paging.last != "") {
            read_links_html += `<li><a href="#" data-page="` + data.paging.last + `"><i class="material-icons">last_page</i></a></li>`;
        }

        read_links_html += `</ul></div>`;
    }

    $("#page-content").html(read_links_html);
}