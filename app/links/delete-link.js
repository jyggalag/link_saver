$(document).on('click', '#delete', function() {
    var link_id = $(this).attr('data-id');

    $(document).on('submit', '#delete-link-form', function() {
        $.ajax({
            type: "delete",
            url: "http://localhost:8000/api/link/delete.php",
            data: JSON.stringify({id: link_id}),
            contentType: "application/json",
            success: function () {
                // location.reload();
                showLinksFirstPage();
                M.toast({html: 'Link deleted!'});
                // console.log(result);
            },
            error: function(xhr, response, text) {
                console.log(xhr, response, text);
                console.warn(xhr.responseText);
            }
        });
        return false;

    });    
});
