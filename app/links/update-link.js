$(document).on('click', '#star', function() {
    var link_id = $(this).attr('data-id');

    $.ajax({
        type: "PUT",
        url: "http://localhost:8000/api/link/update.php",
        data: JSON.stringify({id: link_id}),
        contentType: "application/json",
        success: function (result) {
            M.toast({html: 'Changed starred!'})
            showLinksFirstPage();
        },
        error: function(xhr, response, text) {
            console.log(xhr, response, text);
            console.warn(xhr.responseText);
        }
    });
    return false;
});
