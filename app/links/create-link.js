$(document).on('submit', '#add-link-form', function() {
    var form_data = JSON.stringify($(this).serializeObject());

    $.ajax({
        type: "POST",
        url: "http://localhost:8000/api/link/create.php",
        data: form_data,
        contentType: "application/json; charset=ISO-8859-2",
        success: function (result) {
            $("#title").val('');
            $("#link").val('');
            M.toast({html: 'Link created!'})
            showLinksFirstPage();
        },
        error: function(xhr, response, text) {
            console.log(xhr, response, text);
            console.warn(xhr.responseText);
        }
    });
    return false;
});
