<?php
// required header

// header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once $_SERVER["DOCUMENT_ROOT"]."/config/Database.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/api/objects/Category.php";

$database = new Database();
$db = $database->getConnection();

$category = new Category($db);

$stmt = $category->readCategories();
$num = $stmt->rowCount();



if ($num > 0) {
    $categories_arr = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        $category_item = array(
            "id" => $id,
            "name" => $name
        );

        array_push($categories_arr, $category_item);
    }

//     http_response_code(200);
//     print_r($categories_arr);
//     echo sizeof($categories_arr);
// } else {
//     http_response_code(404);
//     echo json_encode(array("Message: " => "No categories found"));
}