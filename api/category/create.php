<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once $_SERVER["DOCUMENT_ROOT"] . "/config/Database.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/api/objects/Category.php";

$database = new Database();
$db = $database->getConnection();

$category = new Category($db);

$data = json_decode(file_get_contents("php://input"));

if ($data === null) {
    http_response_code(400);
    die(json_encode(array("message" => "Unparsable body")));
}

if (!empty($data->name)) {
    $category->name = $data->name;

    if ($category->createCategory()) {
        http_response_code(201);
        echo json_encode(array("message" => "Created category: " . $category->name . "."));
    } else {
        http_response_code(400);
        echo json_encode(array("message" => "Unable to create category."));
    }
}