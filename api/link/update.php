<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once $_SERVER["DOCUMENT_ROOT"] . "/config/Database.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/api/objects/Link.php";

$database = new Database();
$db = $database->getConnection();

$link = new Link($db);

$data = json_decode(file_get_contents("php://input"));

if ($data === null) {
    http_response_code(400);
    die(json_encode(array("message" => "Unparsable body")));
}

if (!empty($data->id)) {
    $link->id = $data->id;

    if ($link->updateStarredLink()) {
        http_response_code(204);
        echo json_encode(array("message" => "Link updated"));
    } else {
        http_response_code(503);
    } 
} else {
    http_response_code(400);
}