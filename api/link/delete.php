<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow_Methods: DELETE');
header('Access-Control-Allow-Headers: Access-Control-Allow_Methods, Content-Type, Access-Control-Allow_Methods, Authorization, X-Requested-With');

include_once $_SERVER["DOCUMENT_ROOT"] . "/config/Database.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/api/objects/Link.php";

$database = new Database();
$db = $database->getConnection();

$link = new Link($db);

$data = json_decode(file_get_contents("php://input"));

$link->id = $data->id;

if ($link->deleteLink()) {
    http_response_code(204);
    echo json_encode(array("message" => "Link with id: " . $data->id . " has been deleted."));
} else {
    http_response_code(400);
    echo json_encode(array("message" => "Something went wrong."));
}