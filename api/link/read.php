<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once $_SERVER["DOCUMENT_ROOT"]."/config/core.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/config/Utilities.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/config/Database.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/api/objects/Link.php";

$database = new Database();
$db = $database->getConnection();

$link = new Link($db);

$stmt = $link->getLinks($from_record_num, $records_per_page);
$num = $stmt->rowCount();

if ($num > 0) {
    $links_arr = array();
    $links_arr["records"] = array();
    $links_arr["paging"] = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $link_item = array(
            "id" => $id,
            "title" => $title,
            "url" => $url,
            "category_id" => $category_id,
            "category_name" => $category_name,
            "starred" => $starred,
            "added_at" => $added_at
        );

        array_push($links_arr["records"], $link_item);
    }

    // paging
    $total_rows = $link->countLinks();
    $page_url = "{$home_url}/link/read.php?";
    $paging = Utilities::getPaging($page, $total_rows, $records_per_page, $page_url);
    $links_arr["paging"] = $paging;

    http_response_code(200);
    echo json_encode($links_arr);
} else {
    http_response_code(404);
    echo json_encode(array("message" => "No links found"));
}

