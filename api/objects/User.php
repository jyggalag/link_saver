<?php

class User {

    private $conn;
    private $table = "users";

    public $login;
    public $password;
    public $email;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function getUser() {
        $query = "SELECT login, password FROM ".$this->table." WHERE login=:login AND password=:password LIMIT 0,1";
        
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->login = htmlspecialchars(strip_tags($this->login));
        $this->password = htmlspecialchars(strip_tags($this->password));

        // bind values
        $stmt->bindParam(":login", $this->login);
        $stmt->bindParam(":password", $this->password);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->login = $row['login'];
        $this->password = $row['password'];
    }

}