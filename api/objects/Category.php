<?php

class Category {
    private $table = "categories";
    private $conn;

    public $id;
    public $name;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function readCategories() {
        $query = "SELECT id, name FROM " . $this->table . " ORDER BY name";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function createCategory() {
        $query = "INSERT INTO " . $this->table . " SET name=?";
        $stmt = $this->conn->prepare($query);
        
        $this->name = htmlspecialchars(strip_tags($this->name));

        $stmt->bindParam(1, $this->name);

        try {
            $stmt->execute();
        } catch(Exception $e) {
            http_response_code(400);
            die(json_encode(array("message: " => $e)));
        }
        return true;
                
    }
}