<?php

class Link {
    private $conn;
    private $table = "links";

    public $id;
    public $title;
    public $url;
    public $added_at;
    public $starred;
    public $category_id;
    public $category_name;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function createLink() {
        $query = "INSERT INTO " . $this->table . " SET title=:title, url=:url, category_id=:category_id, added_at=:added_at, starred=:starred";
        $stmt = $this->conn->prepare($query);

        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->url = htmlspecialchars(strip_tags($this->url));
        $this->category_id = htmlspecialchars(strip_tags($this->category_id));
        $this->added_at = htmlspecialchars(strip_tags($this->added_at));
        $this->starred = htmlspecialchars(strip_tags($this->starred));
        
        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":url", $this->url);
        $stmt->bindParam(":category_id", $this->category_id);
        $stmt->bindParam(":added_at", $this->added_at);
        $stmt->bindParam(":starred", $this->starred);
        
        if ($stmt->execute()) {
            return true;
        }

        echo json_encode(array("error:" => $stmt->error));
        return false;
    }

    public function deleteLink() {
        $query = "DELETE FROM " . $this->table . " WHERE id=?";
        $stmt = $this->conn->prepare($query);
        $this->id = htmlspecialchars(strip_tags($this->id));
        $stmt->bindParam(1, $this->id);

        if ($stmt->execute()) {
            return true;
        }

        echo json_encode(array("error: " => $stmt->error));
    }

    public function updateStarredLink() {
        // get single link starred status
        $starred_status = 0;
        $query_single_link = "SELECT starred FROM " . $this->table . " WHERE id=?";
        $stmt = $this->conn->prepare($query_single_link);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // check starred
        if ($row["starred"] == 0) {
            $starred_status = 1;
        }
        if ($row["starred"] == 1) {
            $starred_status = 0;
        }

        unset($stmt);

        $query = "UPDATE " . $this->table . " SET starred=:starred WHERE id=:id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":starred", $starred_status);
        
        if ($stmt->execute()) {
            return true;
        }

        echo json_encode(array("error:" => $stmt->error));
        return false;
    }

    public function getLinks($from_record_num, $records_per_page) {
        $query = "SELECT c.name as category_name, l.id, l.title, l.url, l.added_at, l.starred, l.category_id FROM " . $this->table . " l 
                LEFT JOIN categories c ON l.category_id = c.id ORDER BY l.starred DESC, l.added_at DESC LIMIT ?, ?";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

        $stmt->execute();
        return $stmt;
    }

    public function countLinks() {
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table . "";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row['total_rows'];
    }
}