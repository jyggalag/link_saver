<?php

class Footer {

    private $footer;


    public function __construct() {

    }

    public function createFooter() {
        $this->footer = '   
            </div>        
            <!-- Compiled and minified JavaScript -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
            <script src="app/assets/js/jquery.js"></script>

            <script src="app/app.js"></script>

            <script src="app/links/links.js"></script>
            <script src="app/links/read-links.js"></script>
            <script src="app/links/delete-link.js"></script>
            <script src="app/links/create-link.js"></script>
            <script src="app/links/update-link.js"></script>

            <script src="app/categories/create-category.js"></script>

            <script>
                document.addEventListener(\'DOMContentLoaded\', function() {
                    var elems = document.querySelectorAll(\'select\');
                    var instances = M.FormSelect.init(elems);
                });
            </script>
            <script>

            document.addEventListener(\'DOMContentLoaded\', function() {
                var elems = document.querySelectorAll(\'.modal\');
                var instances = M.Modal.init(elems);
            });

            </script>
            </body>
        </html>';
    }

    public function getFooter() {
        return $this->footer;
    }
}