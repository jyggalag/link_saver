<?php

include_once $_SERVER["DOCUMENT_ROOT"]."/api/objects/Link.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/config/Database.php";

class Header {

    private $page_title;
    private $header;

    public function __construct($page_title) {
        $this->page_title = $page_title;
    }

    public function createHeader($user=null) {

        $logged_user = '';

        if ($user != null || $user != '')  {
            $logged_user .= '
            <ul id="nav-mobile" class="left hide-on-med-and-down">
                <li>Logged as: '.$user.' </li>
                <li><a class="waves-effect waves-light btn" href="authorization/logout.php">Logout <i class="material-icons right">exit_to_app</i></a></li>
            </ul>';

            $logged_user .= '</span>
            <span class="right right-my">Today is: ' . self::getDateToday() . ' | there are <b><span id="links-count"></span></b> links</span>';
        }

        $this->header = ' <!DOCTYPE html>
        <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
        <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
        <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
        <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <title>Link Saver - '.$this->page_title.'</title>
                <meta name="description" content="">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <!-- Compiled and minified CSS -->
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
                <!-- icons -->
                <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">      
        
                <link href="app/assets/css/styles.css" rel="stylesheet" />
            </head>
            <body>
                <!--[if lt IE 7]>
                    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
                <![endif]-->
            <nav>
                <div class="nav-wrapper teal">
                    <a href="#" class="brand-logo center">link_Saver - '.$this->page_title.'</a>
                    <span class="left left-my">'.$logged_user.'
                </div>
            </nav>
            <div class="container">
            ';
    }

    public function getHeader() {
        return $this->header;
    }

    private function getDateToday() {
        return date("Y-m-d"); 
    }
}